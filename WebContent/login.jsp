<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Employment Reimbursement System</title>
<link rel="stylesheet" href="main.css" />
</head>
<body>

<%@ include file="banner.jsp"%>


<div id="mainspace">



<script>

function submitForm(form){
	
	var request = new XMLHttpRequest();
	request.open(form.method, form.action, true);
	request.responseType = "text";
	request.onreadystatechange = function() {
		//alert(request.readyState + " " + request.status);
		if (request.readyState == 4 && request.status == 200) {
			//alert("something's happening!");
			document.getElementById("mainspace").innerHTML = request.responseText;
		}
	}
	request.send(new FormData(form));
	//alert("form sent");
	
	return false;
}

</script>


<form action="Login" method="post" name="login form">
<label for="username">Username:</label><br />
<input type="text" placeholder="johndoe123" name="username" /><br />
<label id="pwlabel" for="password">Password:</label><br />
<input type="password" name="password"/><br />
<input type="submit" value="Login"/>


<%if(Boolean.TRUE.equals(request.getAttribute("loginFail"))) {%>
<div id="invalidLogin">incorrect username or password</div>
<%} %>
<%if(Boolean.TRUE.equals(request.getAttribute("loggedOut"))) {%>
<div id="logoutSuccess">You have successfully logged out.</div>
<%} %>
</form>


</div>

<%--
<c:choose>
<c:when test="\"Finance Manager\".equals(session.getAttribute(\"userType\"))">
  <jsp:include page="finance_manager.jsp">
</c:when>
<c:when test="\"Employee\".equals(session.getAttribute(\"userType\"))">
  <jsp:include page="employee.jsp">
</c:when>
<c:when test="null == session.getAttribute(\"userType\")">
  <jsp:include page="login.jsp">
</c:when>
<c:otherwise> this shouldn't happen </c:otherwise>
</c:choose>
<%--Not a whole lot of other info here. --%>
<%--if(session.getAttribute("userType")) --%>
</body>
</html>