<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Employment Reimbursement System</title>
<link rel="stylesheet" href="main.css" />
</head>
<body>

<%@ include file="banner.jsp"%>


<div id="mainspace">
<jsp:include page="login.jsp" />
</div>



<%--
<c:choose>
<c:when test="\"Finance Manager\".equals(session.getAttribute(\"userType\"))">
  <jsp:include page="finance_manager.jsp">
</c:when>
<c:when test="\"Employee\".equals(session.getAttribute(\"userType\"))">
  <jsp:include page="employee.jsp">
</c:when>
<c:when test="null == session.getAttribute(\"userType\")">
  <jsp:include page="login.jsp">
</c:when>
<c:otherwise> this shouldn't happen </c:otherwise>
</c:choose>
<%--Not a whole lot of other info here. --%>
<%--if(session.getAttribute("userType")) --%>
</body>
</html>