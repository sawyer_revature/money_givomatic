<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
<%!static int multiply(int i, int j){
	return i * j;
} %>
<p>Scriptlet: <%multiply(3, 2);%></p>
<p>Expression: <%=multiply(3, 2)%></p>
</body>
</html>